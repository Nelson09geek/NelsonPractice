import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  pageTitle = 'TeamManagement';

  constructor(private _router:Router) { }

  ngOnInit(){ 
    var email_id=window.sessionStorage.getItem("email_id");
      if(email_id)
      { 
        this._router.navigate(['/welcome']); 
      }
      else
      {
        this._router.navigate(['/login']);
      }      
  }
}
