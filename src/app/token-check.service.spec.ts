import { TestBed, inject } from '@angular/core/testing';

import { TokenCheckService } from './token-check.service';

describe('TokenCheckService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenCheckService]
    });
  });

  it('should be created', inject([TokenCheckService], (service: TokenCheckService) => {
    expect(service).toBeTruthy();
  }));
});
