import { Component, OnInit } from '@angular/core';
import { AttendanceListService } from './attendance-list.service';
import { attendanceListDataType } from './attendanceListDataType';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-apply-attendance',
  templateUrl: './apply-attendance.component.html',
  styleUrls: ['./apply-attendance.component.css'],
  providers: [AttendanceListService]
})
export class ApplyAttendanceComponent implements OnInit {

  private attendanceListItems:attendanceListDataType;
  FilteredListItems;
  isClassVisible=true;
  _filterKeyword:string;
  constructor(private _attendanceService:AttendanceListService,private _router:Router) { }

  get listFilter():string{
    return this._filterKeyword;
  }

  set listFilter(value:string){
     this._filterKeyword=value;
     console.log(this.performFilter(this._filterKeyword).length);
    this.FilteredListItems=this._filterKeyword?this.performFilter(this._filterKeyword):this.attendanceListItems.attendanceList;
  }
  
  performFilter(value:string):any[]{

  var localList=this.attendanceListItems.attendanceList;
  return this.attendanceListItems.attendanceList.filter((item:any)=>
   {
      let matchstr_att:string=<string>item.attendance;
      let matchstr_day:string=<string>item.day;
     return (matchstr_att.toLocaleLowerCase().indexOf(value)!==-1 ||matchstr_day.toLocaleLowerCase().indexOf(value)!==-1);
   }  
  );
  }
  ngOnInit() {
    this._attendanceService.getattendanceList().subscribe
    (
        data => {
            console.log(data[0]);
            this.attendanceListItems=data[0];
            this.FilteredListItems=this.attendanceListItems.attendanceList;
             
        }, 
        error => {
            var err=<any>error;
            console.log(err);
        }

    )
  }
  gotoproceedAttendace(){
    this._router.navigate(['/applyAttendance/proceedAttendance']); 
  }

}
