import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { attendanceListDataType } from './attendanceListDataType';

@Injectable()
export class AttendanceListService {
  private _productUrl='http://localhost:3000/attendanceList';
  constructor(private _http:HttpClient) { }

  getattendanceList():Observable<attendanceListDataType[]>{
    var email_id=window.sessionStorage.getItem("email_id");
    return this._http.post<attendanceListDataType[]>(this._productUrl,{
      email_id:email_id
    })
    .do(data => console.log(data))
    .catch(this.handleError);

  }
  handleError(err:HttpErrorResponse){
    console.log(err.message);
    return Observable.throw(err.message);
  } 


}
