import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms'; 
import { ApplyAttendanceComponent } from './apply-attendance/apply-attendance.component';
import { ProceedAttendanceComponent } from './proceed-attendance/proceed-attendance.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path:'applyAttendance',component:ApplyAttendanceComponent},
      {path:'applyAttendance/proceedAttendance',component:ProceedAttendanceComponent}
    ])
  ],
  declarations: [ApplyAttendanceComponent, ProceedAttendanceComponent]
})
export class AttendanceModule { }
