import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceedAttendanceComponent } from './proceed-attendance.component';

describe('ProceedAttendanceComponent', () => {
  let component: ProceedAttendanceComponent;
  let fixture: ComponentFixture<ProceedAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceedAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceedAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
