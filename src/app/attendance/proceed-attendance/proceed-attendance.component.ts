import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-proceed-attendance',
  templateUrl: './proceed-attendance.component.html',
  styleUrls: ['./proceed-attendance.component.css']
})
export class ProceedAttendanceComponent implements OnInit {
  
  InputCity:string="chennai";
  Inputdatevalue:string;
  InputIntime:string;
  InputOuttime:string;
  InputWorkingHourstime:number;
  InputWorkingPlace:string="From Office";
  longitude:any;
  latitude:any;
  i1:number;
  i2:number;
  o1:number;
  o2:number;
   
 

   

//   set Inputdate(value:string){

//     console.log(value);
//     this.Inputdatevalue=value;
//     console.log(this.Inputdatevalue);
    
// }
// get Inputdate():string{
//   return this.Inputdatevalue;
// }
  constructor(private _http:HttpClient,private _router:Router) { }
  
  ngOnInit() {
  } 
  
  
  passAttendancetoBE():void{
    
    
    var email_id=window.sessionStorage.getItem("email_id");
    console.log(this.InputCity);
    console.log(this.Inputdatevalue);
    console.log(this.InputIntime);
    console.log(this.InputOuttime);
    console.log(this.InputWorkingHourstime);
    console.log(this.longitude);
    console.log(this.latitude);
    console.log(this.InputIntime.split(":"));
    this.i1=parseInt((this.InputIntime.split(':'))[0]);  
    this.i2=parseInt((this.InputIntime.split(':'))[1]);
    this.o1=parseInt((this.InputOuttime.split(':'))[0]);  
    this.o2=parseInt((this.InputOuttime.split(':'))[1]);
    console.log(this.o1-this.i1);
    this.InputWorkingHourstime=Math.abs(this.o1-this.i1);
    console.log(this.InputWorkingHourstime);  
    var login_url='http://localhost:3000/applyAttendance';
    this._http.post<any>(login_url,{
      email_id:email_id,
      city:this.InputCity,
      day:this.Inputdatevalue,
      start_hour:this.InputIntime,
      end_hour:this.InputOuttime,
      working_hours:this.InputWorkingHourstime,
      working_from:this.InputWorkingPlace,
      longitude:this.longitude,
      latitude:this.latitude
    })
    .subscribe(
      data => { 
        console.log(data.flag);
        
        if(data.flag=="success")
        {
          
              this._router.navigate(['/applyAttendance']);
              
        }
        else if(data.flag=="error")
        {
            alert("Error while applying attendance,Please Try again");
          
        }
        else if(data.flag=="invalid_location")
        {
          alert("Kindly goto office and then apply the attendance");
          
        }
      },
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );

  }
  markAttendace() {
    
    var that=this;
    if (navigator.geolocation) {
      
      //navigator.geolocation.getCurrentPosition(this.showPosition);
      navigator.geolocation.getCurrentPosition(function(position){
        
        that.longitude=position.coords.longitude;
        that.latitude=position.coords.latitude;
        that.passAttendancetoBE();
      });
     
    } 
    else {
      console.log("Geolocation is not supported by this browser");
      
    }
    
   
  
  }
  showPosition(position) {
      // console.log("Latitude: " + position.coords.latitude + 
      // "Longitude: " + position.coords.longitude);
      var longitude=position.coords.longitude;
      var latitude=position.coords.latitude;
     
    
    console.log("hello");
    console.log("Latitude: " + latitude + 
      "Longitude: " + longitude);
  //console.log(this.Inputdatevalue);
  }
  

}
