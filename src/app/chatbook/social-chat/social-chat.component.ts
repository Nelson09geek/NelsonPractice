import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'pm-social-chat',
  templateUrl: './social-chat.component.html',
  styleUrls: ['./social-chat.component.css']
})
export class SocialChatComponent implements OnInit {
  
  closeResult: string;
  post_array:any;  
  inputPostName:string;
  inputPostDetail:string;
  monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  @ViewChild('id01') modal_id: ElementRef;
  constructor(private _http:HttpClient,private _router:Router) { }
 
  
  createPost(){
    this.modal_id.nativeElement.style.display='none';
    var post_name=this.inputPostName;
    var post_detail=this.inputPostDetail;
    var email_id=window.sessionStorage.getItem("email_id");
    var date1=new Date();
    var final_date=this.monthNames[date1.getMonth()]+" "+date1.getDate()+","+date1.getFullYear();
    console.log(final_date);

    var  url='http://localhost:3000/create_post';
    this._http.post<any>(url,{
      post_name:post_name,
      post_detail:post_detail,
      email_id:email_id,
      posted_on:final_date 

    })
    .subscribe(
      data => {         
        console.log(data);
        if(data.flag=="success")
        {
          this.LoadPosts();
        } 
        
        
      },      
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );
  }
  ngOnInit() {

    this.LoadPosts();
  }
  LoadPosts(){
    var email_id=window.sessionStorage.getItem("email_id");
    var  url='http://localhost:3000/display_social_posts';
    this._http.post<any>(url,{
      email_id:email_id,
    })
    .subscribe(
      data => {         
        console.log(data);
        this.post_array=data;
        
      },      
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {  
          alert(err);
          console.log(err);
        }
        
       
      }
    );
  }

}

