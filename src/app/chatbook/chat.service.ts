import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

const CHAT_URL = 'ws://localhost:3000/ ';

export interface Message {
  own_email_id: string,
  opp_email_id:string,  
	message: string
}

@Injectable()
export class ChatService {

  public messages: Subject<Message>;

	constructor(wsService: WebsocketService) {
		this.messages = <Subject<Message>>wsService
			.connect(CHAT_URL)
			.map((response: MessageEvent): Message => {
				let data = JSON.parse(response.data);
				return {
          own_email_id: data.own_email_id,
          opp_email_id:data.opp_email_id,
					message: data.message
				}
			});
	}

}
