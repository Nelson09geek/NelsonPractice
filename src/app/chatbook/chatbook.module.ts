import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoChatComponent } from './do-chat/do-chat.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SocialChatComponent } from './social-chat/social-chat.component';
import { DropdownDirective } from './social-chat/navDropdown.component';


@NgModule({
  imports: [  
    CommonModule,
    BrowserModule,
    HttpClientModule, 
    FormsModule, 
    RouterModule.forChild([
      {path:'chatbook',component:DoChatComponent},
      {path:'socialbook',component:SocialChatComponent}     
    ])
  ],
  declarations: [DoChatComponent,SocialChatComponent,DropdownDirective]
})
export class ChatbookModule { }
