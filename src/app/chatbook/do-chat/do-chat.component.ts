import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import { ChatService } from '../chat.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-do-chat',
  templateUrl: './do-chat.component.html',
  styleUrls: ['./do-chat.component.css'],
  providers: [ WebsocketService, ChatService ]
})
export class DoChatComponent implements OnInit {

  private online_users_array;
  private filtered_online_users_array;
  private online_users_json;
  private current_opponent_email_id;
  private ongoing_chat;
  private full_chat;
  private to_email_id:string;
  private displayChatName:string;
  private audio;
  private filterKeyword:string;
  private myself=window.sessionStorage.getItem("email_id");
  constructor(private chatService: ChatService,private _http:HttpClient,private _router:Router) { 

    this.audio = new Audio();
    this.audio.src = "./assets/audio/to-the-point.mp3";
    this.audio.load();
    
    chatService.messages.subscribe(msg => {			
      console.log("Response from websocket: ");
      console.log(msg);
      if(msg.own_email_id=="server1")
      {
            console.log("****Message from Server 1******");
           
            var email_id=window.sessionStorage.getItem("email_id");
            var Name=<string>window.sessionStorage.getItem("first_name")+" "+(window.sessionStorage.getItem("last_name")).substring(0,1)+".";
            this.chatService.messages.next({
              own_email_id: email_id,
              opp_email_id:"",
              message: Name
            });
      }
      else if(msg.own_email_id=="server2")
      {
        console.log("****Message from Server 2******"); 
        console.log(msg.message);
        this.full_chat=msg.message;
      } 
      else
      {
        console.log("****Message from "+msg.own_email_id+" ******");
       
        //alert("****Message from "+msg.own_email_id+" ******");
        console.log(msg.message); 
        console.log(this.current_opponent_email_id);
        console.log(msg.own_email_id);
        console.log(msg.opp_email_id);
        var temp_own_email_id=window.sessionStorage.getItem("email_id");
        if(this.current_opponent_email_id==msg.own_email_id)
        {
          this.audio.play();
          this.full_chat=msg.message;
         
          if(typeof(this.full_chat[msg.own_email_id+'%%'+temp_own_email_id])!="undefined")
          {

            this.ongoing_chat=this.full_chat[msg.own_email_id+'%%'+temp_own_email_id];
          }
          else if(typeof(this.full_chat[temp_own_email_id+'%%'+msg.own_email_id])!="undefined")
          {
            this.ongoing_chat=this.full_chat[temp_own_email_id+'%%'+msg.own_email_id]; 
            
          }
        }
        else if(this.current_opponent_email_id==msg.opp_email_id)
        {
          this.full_chat=msg.message;
          
          if(typeof(this.full_chat[msg.opp_email_id+'%%'+temp_own_email_id])!="undefined")
          {

            this.ongoing_chat=this.full_chat[msg.opp_email_id+'%%'+temp_own_email_id];
          }
          else if(typeof(this.full_chat[temp_own_email_id+'%%'+msg.opp_email_id])!="undefined")
          {
            this.ongoing_chat=this.full_chat[temp_own_email_id+'%%'+msg.opp_email_id]; 
            
          }
        }
        else
        {
          this.audio.play();
          this.full_chat=msg.message;
          console.log("increment count");
          var emailId1=msg.own_email_id;
          var emailId2=window.sessionStorage.getItem("email_id");

          if(typeof(this.full_chat[emailId1+'%%'+emailId2])!="undefined")
                    
          {

                      
            var temp_arr=this.full_chat[emailId1+'%%'+emailId2];
            var count=0;
            for(var i=0;i<temp_arr.length;i++)
            {
              if(temp_arr[i].read==false)
              {
                count++;
              }	
            }       
            for(var j=0;j<this.online_users_array.length;j++)
            {
              if(this.online_users_array[j].email_id==emailId1)
              {
                this.online_users_array[j].pending=count;
                this.filtered_online_users_array=this.filterKeyword?this.performFilter(this.filterKeyword):this.online_users_array;
                break;
              }
            }	

          }

          else if(typeof(this.full_chat[emailId2+'%%'+emailId1])!="undefined")
                    
          {
                    
            var temp_arr=this.full_chat[emailId2+'%%'+emailId1];
            var count=0;
            for(var i=0;i<temp_arr.length;i++)
            {
              if(temp_arr[i].read==false)
              {
                count++;
              }	
            }       
            for(var j=0;j<this.online_users_array.length;j++)
            {
              if(this.online_users_array[j].email_id==emailId1)
              {
                this.online_users_array[j].pending=count;
                this.filtered_online_users_array=this.filterKeyword?this.performFilter(this.filterKeyword):this.online_users_array;
                break;
              }
            }	

          }
        }  
      }
      
    
		});

  }
  
  // private message = {
	// 	own_email_id: 'tutorialedge',
	// 	message: 'this is a test message'
  // } 
  
  sendMsg(text:string) {
    console.log(text);
    var email_id=window.sessionStorage.getItem("email_id");
		this.chatService.messages.next({
      own_email_id: email_id,
      opp_email_id:this.to_email_id,
      message: text
    });
		 
  } 
  openChat(emailId:string,displayname:string){

    var own_email_id=window.sessionStorage.getItem("email_id");
    var opp_email_id=emailId;
    this.current_opponent_email_id=emailId;
    console.log(emailId);
    this.to_email_id=emailId;
    this.displayChatName=displayname;
    console.log(this.full_chat);
      if(typeof(this.full_chat[own_email_id+'%%'+opp_email_id])!="undefined")
      {

        this.ongoing_chat=this.full_chat[own_email_id+'%%'+opp_email_id];
      }
      else if(typeof(this.full_chat[opp_email_id+'%%'+own_email_id])!="undefined")
      {
        this.ongoing_chat=this.full_chat[opp_email_id+'%%'+own_email_id]; 
        
      }
      else
      {
        this.ongoing_chat=[];
        alert("No chat exists");
      }
  }


  get listFilter():string{
    return this.filterKeyword;
  }

  set listFilter(value:string){
     this.filterKeyword=value;
     console.log(this.performFilter(this.filterKeyword).length);
    this.filtered_online_users_array=this.filterKeyword?this.performFilter(this.filterKeyword):this.online_users_array;
  }
  
  performFilter(value:string):any[]{

  
  return this.online_users_array.filter((item:any)=>
   {
      let matchstr_att:string=<string>item.name;
      
     return (matchstr_att.toLocaleLowerCase().indexOf(value)!==-1 );
   }  
  );
  }
  ngOnInit() { 

  
    var email_id=window.sessionStorage.getItem("email_id");
    var  url='http://localhost:3000/display_online_users';
    this._http.post<any>(url,{
      email_id:email_id,
    })
    .subscribe(
      data => {         
        console.log(data);
        this.online_users_array=data.array_form;
        console.log(this.online_users_array);
        this.filtered_online_users_array=this.online_users_array;
      },      
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );
  }

}
