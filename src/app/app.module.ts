import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginCheckService } from './login/login-check.service';
import { WelcomeComponent } from './home/welcome.component';
import { TokenCheckService } from './token-check.service';
import { AttendanceModule } from './attendance/attendance.module';
import { ApplyAttendanceComponent } from './attendance/apply-attendance/apply-attendance.component';
import { ProfileModule } from './profile/profile.module';
import { DisplayProfileComponent } from './profile/display-profile/display-profile.component';
import { ChatbookModule } from './chatbook/chatbook.module';
import { DoChatComponent } from './chatbook/do-chat/do-chat.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WelcomeComponent,
    
  ],
  imports: [
   
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'login',component:LoginComponent},
      {path:'applyAttendance',canActivate:[TokenCheckService],component:ApplyAttendanceComponent},
      {path:'displayProfile',canActivate:[TokenCheckService],component:DisplayProfileComponent},
      {path:'chatbook',canActivate:[TokenCheckService],component:DoChatComponent},       
      {path:'welcome',canActivate:[TokenCheckService],component:WelcomeComponent},     
      {path:'',component:LoginComponent},
      {path:'**',component:LoginComponent} 
      

    ]),
    AttendanceModule,
    ProfileModule,
    ChatbookModule
  ],
  providers: [LoginCheckService, TokenCheckService],
  bootstrap: [AppComponent] 
})
export class AppModule { }
