export interface profileDataType{
    email_id:string,
    fullname:string,
    join_date:string,
    dob:string,
    designation:string,
    department:string,
    certified:string,
}