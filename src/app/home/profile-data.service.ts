import { Injectable } from '@angular/core';
import { HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { profileDataType } from './profileDataType';


@Injectable()
export class ProfileDataService {

    private _productUrl='http://localhost:3000/profile'
  constructor(private _http:HttpClient) { }

  getProfile():Observable<profileDataType[]>{
    return this._http.post<profileDataType[]>(this._productUrl,{
      user_id:"514816"
    })
    .do(data => console.log(data))
    .catch(this.handleError);

  }
  handleError(err:HttpErrorResponse){
    console.log(err.message);
    return Observable.throw(err.message);
  } 

}
