import { Component } from '@angular/core';

import {ProfileDataService} from './profile-data.service'; 
import { profileDataType } from './profileDataType';
import { Router } from '@angular/router';

@Component({
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css'],
  providers: [ProfileDataService]
    
})
export class WelcomeComponent {
    public pageTitle: string = 'Welcome';
    profileData:profileDataType;
    private full_name:string;
    constructor(private _productService:ProfileDataService,private _router:Router){

    }

    ngOnInit() {
        var f1=window.sessionStorage.getItem("first_name");
        var l1=window.sessionStorage.getItem("last_name");
       this.full_name=f1+" "+l1;
        //call here app.post('/',func(err,data)) method for checking token and will throw 403 error and will send it out of that page
        this._productService.getProfile().subscribe
        (
            data => {
                console.log(data[0]);
                this.profileData=data[0];
                 
            }, 
            error => {
                var err=<any>error;
                console.log(err);
            }

        )
    }

    clearStorage():void{
        window.sessionStorage.clear();
        this._router.navigate(['/login']);
    }
    gotoAttendace(){
        this._router.navigate(['/applyAttendance']);
       
    }
    gotoProfile(){
        
        this._router.navigate(['/displayProfile']);
    }
}
