import { Component, OnInit } from '@angular/core';
import {HttpClient,HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router'; 
import { loginResponse } from './loginResponse';



@Component({
  selector: 'pm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private login_url='http://localhost:3000/login';
  constructor(private _http:HttpClient,private _router:Router) { }

 
  ngOnInit() {
  }

  loginPressed():void{
    var email_id=((<HTMLInputElement>document.getElementById("emailId")).value).toString();
    var password=((<HTMLInputElement>document.getElementById("passId")).value).toString();
    //alert("Yes its "+email_id);
    
    
    this._http.post<any>(this.login_url,{
      email_id:email_id,
      password:password
    })
    .subscribe(
      data => { 
        console.log(data.flag);
        
        if(data.flag=="success")
        {
          //alert("You are logged in");
              console.log(data.data);
              window.sessionStorage.setItem("email_id",email_id);
              window.sessionStorage.setItem("user_id",data.data.user_id);
              window.sessionStorage.setItem("first_name",data.data.first_name);
              window.sessionStorage.setItem("last_name",data.data.last_name);

              this._router.navigate(['/welcome']);
               
        }
        
      },
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );

  }

}
