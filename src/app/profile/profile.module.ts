import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayProfileComponent } from './display-profile/display-profile.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TokenCheckService } from '../token-check.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild([
      {path:'displayProfile',component:DisplayProfileComponent},
      {path:'editProfile',component:EditProfileComponent}      
    ])
  ],
  declarations: [DisplayProfileComponent, EditProfileComponent]
})
export class ProfileModule { }
