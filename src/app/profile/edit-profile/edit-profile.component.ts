import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  inputPostName:string;
  inputPostDetail:string;
  monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  constructor(private _http:HttpClient,private _router:Router) { }
  createPost(){
     
     var post_name=this.inputPostName;
     var post_detail=this.inputPostDetail;
     var email_id=window.sessionStorage.getItem("email_id");
     var date1=new Date();
     var final_date=this.monthNames[date1.getMonth()]+" "+date1.getDate()+","+date1.getFullYear();
     console.log(final_date);

     var  url='http://localhost:3000/create_post';
     this._http.post<any>(url,{
       post_name:post_name,
       post_detail:post_detail,
       email_id:email_id,
       posted_on:final_date 

     })
     .subscribe(
       data => {         
         console.log(data);
         
       },      
       err => {
         var err=<any>err;
         if(err.status==403)
         {
           alert("Unauthorized");
           console.log(err.status);
           this._router.navigate(['/login']);
         }
         else
         {
           alert(err);
           console.log(err);
         }
         
        
       }
     );
  }
  ngOnInit() {
  }

}
