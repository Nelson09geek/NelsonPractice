import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-display-profile',
  templateUrl: './display-profile.component.html',
  styleUrls: ['./display-profile.component.css']
})
export class DisplayProfileComponent implements OnInit {
  private profileData:any;
  private education_data:any;
  constructor(private _http:HttpClient,private _router:Router) { }

  ngOnInit() {

    var email_id=window.sessionStorage.getItem("email_id");
    var profile_fetch_url='http://localhost:3000/displayProfile';
    this._http.post<any>(profile_fetch_url,{
      email_id:email_id,
      
    })
    .subscribe(
      data => { 
        
        
        if(data.flag=="error")
        {
            alert("Error while Fetching Profile Data.Please Try again");
          
        }
        else
        {
          console.log(data);
          this.profileData=data[0];
          this.education_data=(data[0]).highest_qualification[0];
          
        }
      },
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("Unauthorized");
          console.log(err.status);
          this._router.navigate(['/login']);
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );
  }

}
