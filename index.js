
//***********************************Index.js file for NelsonPractice***********************************
'use strict';

//******Express,helmet,bodyparser and cors Library for Creating And Listening Server********************
var express = require('express');
var cors = require('cors');
var helmet = require('helmet');
var bodyParser=require('body-parser');
var app = express();
var WebSocketServer = require('ws').Server;

app.options('*', cors());
app.use(helmet());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({limit: '50mb',extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use('/', express.static(__dirname + '/public/'));
var server=app.listen(process.env.PORT || 3000,function(session){ //express server listening on 3000 port
  console.log("Started on PORT 3000");
});
var wss = new WebSocketServer({ //new websocket connection for sending notifications
    server: server,
    keepAlive:true,

});
var webSocketIdArr=[];
var online_users={};
var ongoingChat={
					// "nelson@beeceeyes.com%%sajjan@beeceeyes.com":
					// [
								   // {
									// "time":"11PM",
									// "read":false,
									// "sendBy":"nelson@beeceeyes.com",
									// "receivedBy":"sajjan@beeceeyes.com",
									// "message":"Hi just want to talk to you"
								   // }
					// ]
};
//*******************************************************************************************

//***************************************Scheduler*******************************************
var schedule = require('node-schedule');

//*******************************************************************************************

//**************************Connecting the MongoDB Database***********************************
var database="";
var MongoClient = require('mongodb').MongoClient;
var mongo=require('mongodb'); // for converting string to ObjectID
MongoClient.connect("mongodb://localhost:27017/teamManagement", function(err, db) {
  if(!err) {
    console.log("We are connected with MongoDB");
  }
  else {
    console.log(err);
  }
  database=db;
});
//*********************************************************************************************



//***************************************API section*******************************************

////Login check api
app.post('/login',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('login_detail').find({'email_id':req.body.email_id,'password':req.body.password}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				console.log(thresholds);
				if(thresholds.length==1)
				{
					res.json({"flag":"success","data":thresholds[0]});
				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});

//////Profile api
app.post('/profile',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('user_profile_detail').find({"user_id":req.body.user_id}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				//console.log(thresholds);
				if(thresholds.length==1)
				{
					res.json(thresholds);
				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});

//////Attendance List api
app.post('/attendanceList',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('attendance_table').find({"email_id":req.body.email_id}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				//console.log(thresholds);
				if(thresholds.length==1)
				{
					res.json(thresholds);
				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});

//////DisplayProfile api
app.post('/displayProfile',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('profile_detail').find({"email_id":req.body.email_id}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				//console.log(thresholds);
				if(thresholds.length==1)
				{
					res.json(thresholds);
				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});

/////apply Attendance api
app.post('/applyAttendance',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	//console.log(req.body);
	var jsonData={
            "day" : req.body.day,
            "attendance" : "present",
            "att_flag" : true,
            "start_hour" : req.body.start_hour,
            "end_hour" : req.body.end_hour,
            "working_hours" : req.body.working_hours,
			"working_from":req.body.working_from			
    };
	 var longitude=req.body.longitude;
	 var latitude=req.body.latitude;
	// var longitude=73.8050312;
	// var latitude=18.5618509;
	
	var location_query={
     location:
       { $near:
          {
            $geometry: { type: "Point",  coordinates: [ longitude, latitude] },     //73.6821993, 18.5998333 hinjewadi location(longtitude,latitude)
            $minDistance: 0,//in meters
            $maxDistance: 1000 //in meters
          }
       },
	   city:req.body.city  
   };
  // console.log(location_query);
   	if(req.body.working_from=="From Office")
	{
		database.collection('location_detail').find(location_query).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				console.log(thresholds);
				if(thresholds.length==1)
				{
					console.log("Into the valid location.");
					//******************************Apply Attendance*******************///
							database.collection('attendance_table').find({"email_id":req.body.email_id}).toArray(function(err,result){
							if(err){
								console.log(err);
								res.json({"flag":"error"});
							}
							else{
								console.log(result);
								if(result.length==1)
								{
									console.log(result);
									var updated_array=result[0].attendanceList;
									console.log("*****************");
									for(var i=0;i<updated_array.length;i++)
									{
										if(updated_array[i].day==jsonData.day)
										{
											updated_array[i]=jsonData;
											break;
										}
										
									}
									//updated_array.push(jsonData);
									//console.log(updated_array);
									/////////////////////////////
										database.collection('attendance_table').update({'email_id':req.body.email_id},{
											$set:{
												  'attendanceList':updated_array
											}},	
											function(err,data){
											if(err){
												console.log(err);
												res.json({"flag":"error"});
											}
											else{
												console.log(data);
												res.json({"flag":"success"});
											}
										});
									/////////////////////////////
								}
								else
								{
									console.log("Unauthorized access");
									res.status(403).json();
								}
							}
							
						});
					//****************************************************************///
				}
				else
				{
				   console.log("Kindly goto office and then apply the attendance");
				   res.json({"flag":"invalid_location"});
				}
			}
		});	
	
	}
	else
	{
			//******************************Apply Attendance*******************///
							database.collection('attendance_table').find({"email_id":req.body.email_id}).toArray(function(err,result){
							if(err){
								console.log(err);
								res.json({"flag":"error"});
							}
							else{
								console.log(result);
								if(result.length==1)
								{
									console.log(result);
									var updated_array=result[0].attendanceList;
									console.log("*****************");
									for(var i=0;i<updated_array.length;i++)
									{
										if(updated_array[i].day==jsonData.day)
										{
											updated_array[i]=jsonData;
											break;
										}
										
									}
									// updated_array.push(jsonData);
									//console.log(updated_array);
									/////////////////////////////
										database.collection('attendance_table').update({'email_id':req.body.email_id},{
											$set:{
												  'attendanceList':updated_array
											}},	
											function(err,data){
											if(err){
												console.log(err);
												res.json({"flag":"error"});
											}
											else{
												console.log(data);
												res.json({"flag":"success"});
											}
										});
									/////////////////////////////
								}
								else
								{
									console.log("Unauthorized access");
									res.status(403).json();
								}
							}
							
						});
						
	}
	
	
});

////***************Display Online Users*********************/////
app.post('/display_online_users',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	console.log("****************************************************************************");	
	console.log(online_users);	
	var email_id=req.body.email_id;
	var send_user_data=[]; 
	
	for(var i in online_users)
	{
		if(email_id!=i)
		{		
			send_user_data.push({name:online_users[i],email_id:i,pending:0});
		}
	}
	
	
	
	  console.log(send_user_data);
	  
	// if (!online_users.keys) {
    // online_users.keys = function (obj) {
        // var keys = [],values=[],
            // k;
        // for (k in obj) {
            // if (online_users.prototype.hasOwnProperty.call(obj, k)) {
                // keys.push(k);
				// values.push(obj[k]);
            // }
        // }
		// console.log(keys);
		// console.log(values);
        // return keys;
    // };
//}
	res.json({"obj_form":online_users,"array_form":send_user_data});
	//res.json(online_users);
		
});

//////Social Posts List api
app.post('/display_social_posts',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('social_book_table').find({},{"sort":{$natural:-1}}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				//console.log(thresholds);
				res.json(thresholds);
				
			}
			
		});
		
});

//////Create Social Post api
app.post('/create_post',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	database.collection('profile_detail').find({"email_id":req.body.email_id}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				//console.log(thresholds);
				if(thresholds.length==1)
				{
					//console.log(thresholds);
					
					var post_name=req.body.post_name;
					var post_detail=req.body.post_detail;
					var post_by_email_id=req.body.email_id;
					var post_by_name=thresholds[0].first_name+" "+thresholds[0].last_name;
					var post_img_src=thresholds[0].imageSrc;
					var posted_on=req.body.posted_on;
					
					var json={
							"post_name" : post_name,
							"post_detail" : post_detail,
							"post_by_email_id" : post_by_email_id,
							"post_by_name" : post_by_name,
							"post_img_src" : post_img_src,
							"posted_on" : posted_on,
							"post_like" : [],
							"post_comment" : [],
							"post_share" : []
					};
					console.log(json);
					database.collection('social_book_table').insert(json,function(err,data){
						if(err){
								console.log(err);
								res.json({"flag":"error"});
						}
						else{
								console.log(data);
								res.json({"flag":"success"});
						}
					});

				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});



function heartbeat() { // setting flag true for live websocket connections
  this.isAlive = true;
}


//********DailyAttendance function will add one attendance entry for each user at 12:05 AM daily******
function DailyAttendance()
{
		 	  	
	 var yyyy=(new Date()).getFullYear();
	 var mm1=(new Date()).getMonth() + 1;
	 var dd1=(new Date()).getDay() - 1;	 
	 var final_date="",dd,mm;
	 if(dd1.toString().length==1)
	 {
		dd="0"+dd1;
	 }
	 if(mm1.toString().length==1)
	 {
		mm="0"+mm1;
	 }
	 final_date=yyyy+'-'+mm+'-'+dd;
	// console.log(final_date);
	schedule.scheduleJob("00 05 12 */1 * * *",function(){
		console.log("Time for tea");
		database.collection('attendance_table').update({},{$push:{"attendanceList":{
            "day" : final_date,
            "attendance" : "absent",
            "att_flag" : false,
            "start_hour" : "00:00",
            "end_hour" : "00:00",
            "working_hours" : 0,
            "working_from" : "Special NA"
        }
		}
		},
		{upsert:true,multi:true},
	
				
		function(err,data){
		
		if(err)
		{
		console.log(err);
		}
		else{
			 console.log("nonono");
			 //console.log(data);
				console.log("Daily Attendance has been updated");
		
		}		
				
		
		});
	
	});
	 								
}

//**************************Connecting websocket function********************************************* 
// function connectWebsocket()
// {
try{
wss.on('connection', function connection(ws) { // saving connection in once new connection is created
	  ws.isAlive=true;
	  ws.on('pong', heartbeat);
	  console.log("websocekt connection opened");
	  // console.log(online_users);
	 ws.send(JSON.stringify(
	{own_email_id: "server1",opp_email_id:"",message: "excuse message"}
	));
	  ws.on('message', function incoming(message) {
		// console.log("----------------------");
		// console.log(message);
		// console.log("--------------------");
		if(typeof(message)!= 'undefined')
		{
				var data=JSON.parse(message);
			   if(typeof(data)!= 'undefined')
			   {
						
						if(typeof(data.own_email_id)!= 'undefined' && data.opp_email_id== '')
					   {
						  //ws.uniqueId=data.text;//c0000pranesh
						  console.log("*********************");
						  console.log(data.own_email_id+" has sent first message");						  
						  webSocketIdArr[data.own_email_id]=ws; // storing connnection in websocket bucket
						  //online_users.push([data.own_email_id,data.message]);
						   online_users[data.own_email_id]=data.message;
						  console.log(online_users);
						   var full_chat={};
						  for(var j in ongoingChat)
						  {
								if(j.indexOf(data.own_email_id)!=-1 )
								{		
									full_chat[j]=ongoingChat[j];
								}
						  }
						  ws.send(JSON.stringify(
						  {own_email_id: "server2",opp_email_id:"",message: full_chat}
						   ));
						  
					   }
					   else if(typeof(data.own_email_id)!= 'undefined' && data.opp_email_id!='' && typeof(data.opp_email_id)!='undefined')
					   {
						  //ws.uniqueId=data.text;//c0000pranesh
						  //send message to opp_email_id:-- use webSocketIdArr[opp_email_id]
						  console.log("*********************");
						  console.log(data.own_email_id+" has started messages");
						 console.log(data.opp_email_id);
						  if(typeof(ongoingChat[data.own_email_id+"%%"+data.opp_email_id])!= 'undefined')
						  {
								console.log("reached here 1");		
								var temp_msg=ongoingChat[data.own_email_id+"%%"+data.opp_email_id];
								  temp_msg.push({
											"time":new Date(),
											"read":false,
											"sendBy":data.own_email_id,
											"receivedBy":data.opp_email_id,
											"message":data.message
										   });
								  ongoingChat[data.own_email_id+"%%"+data.opp_email_id]=temp_msg;	
								 //console.log(temp_msg);
								 //console.log(temp_arry);		
								  sendMessagetoEmaidId(data.own_email_id,data.opp_email_id,temp_msg,ongoingChat);
						  }
						  else if(typeof(ongoingChat[data.opp_email_id+"%%"+data.own_email_id])!= 'undefined')
						  {
						  console.log("reached here 2");		
								  var temp_msg=ongoingChat[data.opp_email_id+"%%"+data.own_email_id];
								  temp_msg.push({
											"time":new Date(),
											"read":false,
											"sendBy":data.own_email_id,
											"receivedBy":data.opp_email_id,
											"message":data.message
										   });
								  ongoingChat[data.opp_email_id+"%%"+data.own_email_id]=temp_msg;	
								 //console.log(temp_msg);
								 //console.log(temp_arry);		
								  sendMessagetoEmaidId(data.own_email_id,data.opp_email_id,temp_msg,ongoingChat);
						  }
						  else
						  {
								ongoingChat[data.own_email_id+"%%"+data.opp_email_id]=[];
						       var temp_msg=ongoingChat[data.own_email_id+"%%"+data.opp_email_id];
								  temp_msg.push({
											"time":new Date(),
											"read":false,
											"sendBy":data.own_email_id,
											"receivedBy":data.opp_email_id,
											"message":data.message
										   });
								  ongoingChat[data.own_email_id+"%%"+data.opp_email_id]=temp_msg;	
								 //console.log(temp_msg);
								 //console.log(temp_arry);		
								  sendMessagetoEmaidId(data.own_email_id,data.opp_email_id,temp_msg,ongoingChat);
						  }
						 
						  
					   }
					   else
					   {
						console.log("Invalid chat to undefined");
					   }
			   }
		}
	  });
	  
	  ws.on('error', () => console.log('client is disconnected'));
});
wss.clients.forEach(function each(ws) {
    //console.log(ws.uniqueId);
        if (ws.isAlive == false) // condition to check if connection is  active or not
        {
        //console.log("Disconnection happened.Session Expired");
        return ws.terminate(); // terminate connection if connection is not active
        }
        else
        {
        //console.log("Conncection is already placed");
          //user_session[ws.uniqueId]=1;
        }
    ws.isAlive = false;
    ws.ping('', false, true);
});
}
catch(e)
{
  console.log(e);
}
//}
//**********************************Send Message to opp_Email_Id from own_email_id********************************
function sendMessagetoEmaidId(own_email_id,opp_email_id,message,ongoingChat)
{
					if(typeof(webSocketIdArr[opp_email_id]) != 'undefined')
                    {
                      if(webSocketIdArr[opp_email_id].readyState==webSocketIdArr[opp_email_id].OPEN)
                      {
                        try {
							console.log("************Sending finally**************");
							
							 var full_chat_own={};
							 var full_chat_opp={};
							  for(var j in ongoingChat)
							  {
									if(j.indexOf(own_email_id)!=-1 )
									{		
										full_chat_own[j]=ongoingChat[j];
									}
							  }
							  for(var k in ongoingChat)
							  {
									if(k.indexOf(opp_email_id)!=-1 )
									{		
										full_chat_opp[k]=ongoingChat[k];
									}
							  }
							//console.log({own_email_id: own_email_id,opp_email_id:opp_email_id,message: full_chat_own});
							console.log("*******Chat till now***************");
							console.log(full_chat_own);
							console.log(full_chat_opp);
							console.log(ongoingChat);
							webSocketIdArr[own_email_id].send(JSON.stringify(                            
							{own_email_id: own_email_id,opp_email_id:opp_email_id,message: full_chat_own}
                            ));
                            webSocketIdArr[opp_email_id].send(JSON.stringify(                            
							{own_email_id: own_email_id,opp_email_id:opp_email_id,message: full_chat_opp}
                            ));
							
							

						} catch (e) {
							  console.log(e);
							  
						}
                      }
                      else {
                        console.log("Opp user "+opp_email_id+"  is offline")
                      }
                    }
}


//DailyAttendance() 
// connectWebsocket()



